class AddressBook:
    def __init__(self, book):
        self.book = book


    def user_add(self):
        name = input('Name: ').lower()
        number = input('Number: ')
        if name and number != '':
            self.book[name] = number
        else:
            pass

    def user_del(self):
        del_elem = input('Who you wanna delete? Enter name: ').lower()
        if del_elem != '':
            del self.book[del_elem]
        else:
            pass

    def watch_book(self):
        print('Name:     Number:')
        for key, value in sorted(self.book.items()):
            print(f'{key.title()} - {value}')

    def user_edit(self):
        edit_elem = input('Whos you wanna edit? Enter name: ').lower()
        if edit_elem not in self.book.keys():
            print('You dont have what contact yet.')
        else:
            for k, v in self.book.items():
                if edit_elem == k:
                    print(f'Name: \n - {k.title()}\nNum:\n - {v}')
            what_change = input('What you wanna change? 1 - name, 2 - number: ')
            if what_change == '1':
                changing_item = input('Enter new name: ').lower()
                self.book[changing_item] = self.book[edit_elem]
                del self.book[edit_elem]
            elif what_change == '2':
                changing_item = input('Enter new number: ')
                self.book[edit_elem] = changing_item

    def find_usr(self):
        search_q = input('Whos you wanna find? ').lower()
        if search_q in self.book.keys():
            for k, v in self.book.items():
                if search_q == k:
                    print(f'Name: \n - {k.title()}\nNum:\n - {v}')
        else:
            print('You dont have what contact.')



