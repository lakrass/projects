import json
from AB import AddressBook

def get_book():
    global filename
    filename = 'book_dump.json'
    global file
    try:
        with open(filename) as book_dump:
            file = json.load(book_dump)
    except FileNotFoundError:
        file = {}
    else:
        return file


greetings = 'Welcome to AB. AB - this is Adress Book program where you can '
greetings += 'add, delete, edit, search  or watch your contact list.\n'
print(greetings)
working = True

book = AddressBook(get_book())

while working:
    user_input = input('a - add, e - edit,\
 d - delete, s - search, w - watch, q - QUIT: ')

    try:
        if user_input == 'q':
            working = False
        if user_input == 'a':
            book.user_add()
        if user_input == 'e':
            book.user_edit()
        if user_input == 'd':
            book.user_del()
        if user_input == 'w':
            book.watch_book()
        if user_input == 's':
            book.find_usr()
        with open(filename, 'w') as save:
            json.dump(file, save, indent=4)
    except Exception:
        file = {}


