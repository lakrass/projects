// Demonstrate autoboxing/unboxing.
class AutoBox {
    public static void main(String[] args) {
        Integer iObj = 100; // autobox an int to Ingeter

        int i = iObj; // auto-unbox

        System.out.println(i + " " + iObj);
    }
}
