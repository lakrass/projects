/* An improved version of the "Decision Makes"
   program from Chapter 9. This verison uses an
   enum, rather than interface variables, to
   represent the answers.
*/

import java.util.Random;

enum Answers {
    NO, YES, MAYBE, LATER, SOON, NEVER
}

class Question {
    Random rand = new Random();

    Answers ask() {
        int prob = (int) (100 * rand.nextDouble());

        if (prob < 15)
            return Answers.MAYBE;
        else if (prob < 30)
            return Answers.NO;
        else if (prob < 60)
            return Answers.YES;
        else if (prob < 75)
            return Answers.LATER;
        else
            return Answers.NEVER;
    }
}

class AskMe {
    static void answer(Answers result) {
        switch (result) {
        case NO:
            System.out.println("no");
            break;
        case YES:
            System.out.println("yes");
            break;
        case MAYBE:
            System.out.println("maybe");
            break;
        case LATER:
            System.out.println("later");
            break;
        case SOON:
            System.out.println("soon");
            break;
        case NEVER:
            System.out.println("never");
            break;
        }
    }
    
    public static void main(String[] args) {
        Question q = new Question();
        answer(q.ask());
        answer(q.ask());
        answer(q.ask());
        answer(q.ask());
        answer(q.ask());
    }
}
