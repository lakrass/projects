// Autoboxing/unboxing takes place with
// method parameters and return values.
class AutoBox2 {
    // Take an Integer param and return an int value
    static int m(Integer v) {
        return v; // auto-unbox to int
    }

    public static void main(String[] args) {
        /*
          Pass an int to m() and assign the return value
          to an Integer. Here, the argument 100 is autoboxed
          into an Integer. Ther return value is alos autoboxed
          into an Integer.
        */
        Integer iObj = m(100);

        System.out.println(iObj);
    }
}
