// Use an enum constructor, instance variable, and method.
enum Apple {
    Jonathan(10), GoldenDel, RedDel(12), Winesap(15), Cortland(8);

    private int price; // price of each apple
    
    // default constructor
    Apple() {
        this.price = -1;
    }
    
    Apple(int price) {
        this.price = price;
    }

    int getPrice() {
        return this.price;
    }
}

class EnumDemo3 {
    public static void main(String[] args) {
        Apple ap; // declaring the variable to invoke Apple constructor,
                  // so each apple get own price.

        // Display price of Winesap.
        System.out.println("Winesap costs " +
                Apple.Winesap.getPrice() +
                " cents.\n");

        // Display all apples and prices.
        System.out.println("All apple prices:");
        for(Apple a: Apple.values())
            System.out.println(a + " costs " + a.getPrice() + " cents.");
        

    }
}
