class TestIface2 {
    public static void main(String[] args) {
        Callback c = new Client();
        AnotherClient obj = new AnotherClient();

        c.callback(42);

        c = obj; // now refers to AnotherClients object
        c.callback(42);
    }
}
