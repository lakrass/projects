interface IntStack {
    void push(int item); // store an item

    int pop(); // retrieve an item

    private int[] getElements(int n) {
        int[] elements = new int[n];

        for(int i = 0; i < n; i++)
            elements[i] = this.pop();
        return elements;
    }

    default void clear() {
        System.out.println("clear() is not implemented.");
    }
    // A method that returns an array that contains
    // the top n elements on the stack.
    default int[] popNElements(int n) {
        return getElements(n);
    }

    // A method that returns an array that contains
    // the next n elements on the stack after skipping
    // elements.
    default int[] skipAndPopNElements(int skip, int n) {
        // Skip the specified number of elements.
        getElements(skip);

        // Return the requested elements.
        return getElements(n);
    }
 }
