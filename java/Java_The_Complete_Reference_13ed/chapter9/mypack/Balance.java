package mypack;

/*
  Now, the Balance class, its constructor, and its
  show() method are public. This means that tehy can be used
  by non-subclass code outside their package.
*/

public class Balance {
    String name;
    double balance;

    public Balance(String name, double balance) {
        this.name = name;
        this.balance = balance;
    }

    public void show() {
        if(balance < 0)
            System.out.print("--> ");
        System.out.println(name + ": $" + balance);
    }
}
