public interface MyIF {
    // this is a "normal" interface method declaration.
    int getNumber();

    // this is default method impl
    default String getString() {
        return "Default String";
    }
    
    // this is static interface method.
    static int getDefaultNumber() {
        return 0;
    }
    // can be called as:
    // int defNum = MyIF.getDefaultNumber();
}
