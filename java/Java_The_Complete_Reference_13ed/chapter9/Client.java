class Client implements Callback {
    //implement callback's interface
    public void callback(int i) {
        System.out.println("callback called with " + i);
    }

    void nonifaceMeth() {
        System.out.println("Classes that implement interfaces " +
                "may also define other members, too.");
    }
}

