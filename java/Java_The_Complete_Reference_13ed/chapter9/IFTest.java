// An implementation of IntStach that uses fixed storage.
class FixedStack implements IntStack {
    private int[] stack;
    private int tos;

    FixedStack(int size) {
        this.stack = new int[size];
        this.tos = -1;
    }

    public void push(int item) {
        if (tos == stack.length - 1)
            System.out.println("Stack is full.");
        else
            stack[++tos] = item;
    }

    public int pop() {
        if (tos < 0)
            return 0;
        else
            return stack[tos--];
    }
}

class IFTest {
    public static void main(String[] args) {
        FixedStack stack1 = new FixedStack(5);
        FixedStack stack2 = new FixedStack(8);

        // push some numbers onto the stacks
        for (int i = 0; i < 5; i++) {
            stack1.push(i);
        }

        for (int i = 10; i < 18; i++) {
            stack2.push(i);
        }

        // pop numbers off the stack
        System.out.println("Stack in mystack1:");
        for (int i = 0; i < 5; i++) {
            System.out.print(stack1.pop() + " ");
        }

        System.out.println();
        System.out.println("Stack in mystack2:");
        for (int i = 0; i < 8; i++) {
            System.out.print(stack2.pop() + " ");
        }
        System.out.println();
    }
}
