class IFTest3 {
    public static void main(String[] args) {
        IntStack mystack; // create an interface reference variable
        FixedStack fs = new FixedStack(8);
        DynStack ds = new DynStack(5);

        mystack = ds;
        for (int i = 0; i < 24; i++)
            mystack.push(i);

        mystack = fs;
        for (int i = 0; i < 8; i++)
            mystack.push(i);

        mystack = ds;
        System.out.println("Values of dynamic stack:");
        for (int i = 0; i < 24; i++)
            System.out.print(mystack.pop() + " ");

        System.out.println();

        mystack = fs;
        System.out.println("Values of fixed stack:");
        for (int i = 0; i < 8; i++)
            System.out.print(mystack.pop() + " ");

        System.out.println();
    }
}
