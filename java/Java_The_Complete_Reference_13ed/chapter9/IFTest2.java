class DynStack implements IntStack {
    private int[] stack;
    private int tos;

    DynStack(int size) {
        this.stack = new int[size];
        this.tos = -1;
    }

    public void push(int item) {
        // if stack is full, allocate a larger stack ( stack.length * 2 )
        if (tos == stack.length - 1) {
            int[] temp = new int[stack.length * 2];
            for(int i = 0; i < stack.length; i++)
                temp[i] = stack[i];
            stack = temp;
            stack[++tos] = item;
        }
        else
            stack[++tos] = item;
    }

    public int pop() {
        if (tos < 0)
            return 0;
        else
            return stack[tos--];
    }
}

class IFTest2 {
    public static void main(String[] args) {
        DynStack stack1 = new DynStack(5);
        DynStack stack2 = new DynStack(8);

        // these loop cause each stack to grow
        for (int i = 0; i < 15; i++)
            stack1.push(i);

        for (int i = 10; i < 25; i++)
            stack2.push(i);

        // pop off our stack
        System.out.println("stack1 :");
        for (int i = 0; i < 15; i++)
            System.out.print(stack1.pop() + " ");

        System.out.println();

        System.out.println("stack2 :");
        for (int i = 0; i < 15; i++)
            System.out.print(stack2.pop() + " ");

        System.out.println();
    }
}
