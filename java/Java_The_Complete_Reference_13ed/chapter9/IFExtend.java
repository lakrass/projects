// One inter can extend another
interface A {
    void meth1();

    void meth2();
}

// B now includes meth1() and meth2() -- it ONLY ADDS meth3();
interface B extends A {
    void meth3();
}

// This class MUST implement all of A and B
class MyClass implements B {
    public void meth1() {
        System.out.println("meth1 impl");
    }

    public void meth2() {
        System.out.println("meth2 impl");
    }

    public void meth3() {
        System.out.println("meth3 impl");
    }
}

class IFExtend {
    public static void main(String[] args) {
        MyClass obj = new MyClass();

        obj.meth1();
        obj.meth2();
        obj.meth3();
    }
}
