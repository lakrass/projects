class AnotherClient implements Callback {
    // implement callback's interface
    public void callback(int i) {
        System.out.println("Another version of callback");
        System.out.println("i squared is " + (i * i));
    }
}
