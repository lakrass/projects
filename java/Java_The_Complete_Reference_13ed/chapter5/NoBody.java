class NoBody {
    // find midpoint between i and j
    public static void main(String[] args) {
        int i, j;

        i = 100;
        j = 200;

        while (++i < --j); // while loop have no body

        System.out.println("Midpoint is " + i);
    }
}
