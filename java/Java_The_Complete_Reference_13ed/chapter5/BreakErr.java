class BreakErr {
    public static void main(String[] args) {
        one: for (int i = 0; i < 5; i++) {
            System.out.println("Pass " + i + ": ");
        }

        for (int j = 0; j < 100; j++) {
            if (j == 12)
                break one; // WRONG
            System.out.print(j + " ");
        }
    }
}
