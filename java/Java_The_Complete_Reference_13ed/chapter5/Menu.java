class Menu {
    // using a do-while loop to process a menu selection
    public static void main(String[] args)
    throws java.io.IOException {
        char choice;

        do {
            System.out.println("Help on: ");
            System.out.println("  1. if");
            System.out.println("  2. switch");
            System.out.println("  3. while");
            System.out.println("  4. do-while");
            System.out.println("  5. for\n");
            System.out.println("Choose one: ");
            choice = (char) System.in.read();
        } while (choice < '1' || choice > '5');

        System.out.println("\n");

        switch (choice) {
            case '1':
                break;
            case '2':
                System.out.println("if placeholder");
                break;
            case '3':
                System.out.println("switch placeholder");
                break;
            case '4':
                System.out.println("do-while placeholder");
                break;
            case '5':
                System.out.println("for placeholder");
                break;
        }
    }
}
