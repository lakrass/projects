class MultByTwo {
    public static void main(String[] args) {
        // Left shifting as a quick way to multiply by 2.
        int i;
        int num = 0xFFFFFFE;

        for(i = 0; i < 4; i++) {
            num <<= 1;
            System.out.println(num);
        }
    }
}
