class Array {
	public static void main(String[] args) {
		int[] month_days = new int[12];

		for (int i = 0; i < 12; i++) {
			if (i == 1)
				month_days[i] = 28;
			else if (i == 3 || i == 5 || i == 8 || i == 10)
				month_days[i] = 30;
			else
				month_days[i] = 31;
		}
		System.out.println("April has " + month_days[3] + " days.");
	}
}
