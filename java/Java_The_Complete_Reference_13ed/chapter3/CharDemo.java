class CharDemo {
	public static void main(String[] args) {
		char ch1, ch2;

		ch1 = 88; // code for X
		ch2 = 'Y';

		System.out.println("ch1 is: " + ch1);
		System.out.println("ch2 is: " + ch2);
	}
}
