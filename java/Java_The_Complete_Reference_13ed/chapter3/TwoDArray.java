class TwoDArray {
    public static void main(String[] args) {
	/*
	  When you allocate memory for a multidimensional array,
	  you need only specify the memory for the first (leftmost)
	  dimension. You can allocate the remaining dimensions
	  separately.

	  int[][] twoD = new int[4][];
	  twoD[0] = new int[1];
	  twoD[1] = new int[2];
	  twoD[2] = new int[3];
	  twoD[3] = new int[4];
	 */
	int[][] twoD = new int[4][5];
	int k = 0;

	for(int i = 0; i < twoD.length ; i++)
	    for(int j = 0; j < twoD[0].length; j++) {
		twoD[i][j] = k;
		k++;
	    }

	for(int i = 0; i < twoD.length ; i++) {
	    for(int j = 0; j < twoD[0].length ; j++) 
		System.out.print(twoD[i][j] + " ");
		System.out.println();
	}
    }
}

       
