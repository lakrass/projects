class Scope {
	// Demonstrate block scope.
	public static void main(String[] args) {
		int x;

		x = 10;
		if (x == 10) { // Starting new scope
			int y = 20; // known only to this block

			// x and y both known
			System.out.println("x and y: " + x + " " + y);
			x = y * 2;
		}
		// y = 100; // Error! y not knowh here

		System.out.println("x is: " + x);
	}
}
