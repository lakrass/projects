class VarDemo {
    public static void main(String[] args) {
        var avg = 10.0;
        // use type inference to determine the type of the variable
        System.out.println("value of avg: " + avg);

        //here var is simply a user-defined variable name
        int var = 1;
        System.out.println("value of var: " + var);

        //here var is used as both the type of declaration AND as a variable
        //name
        var k = -var;
        System.out.println("value of k: " + k);
    }
}
