class Test {
    int a, b;

    Test(int i, int j) {
        this.a = i;
        this.b = j;
    }

    //pass an object
    void meth(Test obj) {
        obj.a *= 2;
        obj.b /= 2;
    }
}

class CallByReference {
    public static void main(String[] args) {
        Test obj = new Test(15, 20);

        System.out.println("obj.a and obj.b before the call: ");
        System.out.println(obj.a + " " + obj.b);

        obj.meth(obj);

        System.out.println("obj.a and obj.b after the call: ");
        System.out.println(obj.a + " " + obj.b);
    }
}
