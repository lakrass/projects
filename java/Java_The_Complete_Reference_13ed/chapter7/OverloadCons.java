class Box {
    double width, height, depth;

    Box() {
        this.width = 10;
        this.height = 10;
        this.depth = 10;
    }

    Box(double i) {
        this.width = i;
        this.height = i;
        this.depth = i;
    }

    Box(double width, double height, double depth) {
        this.width = width;
        this.height = height;
        this.depth = depth;
    }

    double volume() {
        return width * height * depth;
    }
}

class OverloadCons {
    public static void main(String[] args) {
        Box mybox2 = new Box();
        Box mycube = new Box(8);
        Box mybox1 = new Box(10, 20, 15);

        System.out.println("Volume of mybox1 is: " + mybox1.volume());
        System.out.println("Volume of mybox2 is: " + mybox2.volume());
        System.out.println("Volume of mycube is: " + mycube.volume());
    }
}
