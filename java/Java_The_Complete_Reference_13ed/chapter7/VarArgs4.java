// varargs, overloading, and ambiguity.
//
// This program contains an error and will
// not compile!
class VarArgs4 {
    static void vaTest(int... v) {
        System.out.println("Classic");
    }

    static void vaTest(boolean... v) {
        System.out.println("Classic boolean");
    }

    public static void main(String[] args) {
        vaTest(1, 3, 5); // OK 
        vaTest(true, true, false, false); // OK

        vaTest(); // Error:Ambiguous!
    }
}
