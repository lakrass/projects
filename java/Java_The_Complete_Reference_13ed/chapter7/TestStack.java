class TestStack {
    public static void main(String[] args) {
        Stack mystack1 = new Stack();
        Stack mystack2 = new Stack();

        // push some numbers onto the stacks
        for (int i = 0; i < 10; i++)
            mystack1.push(i);
        for (int i = 20; i < 30; i++)
            mystack2.push(i);

        // pop some number off the stack
        System.out.println("Stack in mystack1: ");
        for (int i = 0; i < 10; i++)
            System.out.print(mystack1.pop() + " ");

        System.out.println();

        System.out.println("Stack in mystack2: ");
        for (int i = 0; i < 10; i++)
            System.out.print(mystack2.pop() + " ");

        System.out.println();
    }
}
