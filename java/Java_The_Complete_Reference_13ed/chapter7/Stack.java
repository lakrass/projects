//This class defines an integer stack that can hold up to 10 values.
class Stack {
    private int[] stack = new int[10];
    private int tos;

    // Initialize top-of-stack
    Stack() {
        this.tos = -1;
    }

    void push(int i) {
        if (tos == 9)
            System.out.println("Stack if full.");
        else
            stack[++tos] = i;
    }

    int pop() {
        if(tos < 0) {
            System.out.println("Stack is empty!");
            return 0;
        }
        else
            return stack[tos--];
    }
}

