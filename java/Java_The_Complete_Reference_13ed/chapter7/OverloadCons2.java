class Box {
    double width, height, depth;

    // Notice this constructor. It takes an object of type Box.
    Box(Box obj) {
        this.width = obj.width;
        this.height = obj.height;
        this.depth = obj.depth;
    }

    Box(double w, double h, double d) {
        this.width = w;
        this.height = h;
        this.depth = d;
    }

    Box() {
        this.width = 10;
        this.height = 10;
        this.depth = 10;
    }

    Box(double len) {
        this.width = this.height = this.depth = len;
    }

    double volume() {
        return width * height * depth;
    }
}

class OverloadCons2 {
    public static void main(String[] args) {
        Box mybox1 = new Box(10, 20, 15);
        Box mybox2 = new Box();
        Box mycopy = new Box(mybox1);
        Box mycube = new Box(8);

        System.out.println("volume of mybox1: " + mybox1.volume());
        System.out.println("volume of mybox2: " + mybox2.volume());
        System.out.println("volume of mycube: " + mycube.volume());
        System.out.println("volume of mycopy: " + mycopy.volume());
    }
}
