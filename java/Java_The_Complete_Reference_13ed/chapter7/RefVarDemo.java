class MyClass {
    private int i;

    MyClass(int k) {
        this.i = k;
    }

    int geti() {
        return this.i;
    }

    void seti(int k) {
        if (k >= 0)
            this.i = k;
    }
}

    class RefVarDemo {
        public static void main(String[] args) {
            var mc = new MyClass(10);

            System.out.println("Value of i in mc is: " + mc.geti());
            mc.seti(1488);
            System.out.println("Value of i in mc is now: " + mc.geti());
        }
    }
