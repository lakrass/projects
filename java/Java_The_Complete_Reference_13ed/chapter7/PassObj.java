class Test {
    int a, b;

    Test(int a, int b) {
        this.a = a;
        this.b = b;
    }

    //return true if obj is equal to the invoking object
    boolean equalTo(Test obj) {
        return obj.a == this.a && obj.b == this.b;
    }
}

class PassObj {
    public static void main(String[] args) {
        Test ob1 = new Test(100, 22);
        Test ob2 = new Test(100, 22);
        Test ob3 = new Test(-1, -1);

        System.out.println("ob1 == ob2: " + ob1.equalTo(ob2));
        System.out.println("ob1 == ob3: " + ob1.equalTo(ob3));
    }
}
