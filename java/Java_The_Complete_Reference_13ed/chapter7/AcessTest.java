class Test {
    int a; // default acess a.k.a public to members of it package
    public int b; // public acess
    private int c; // private acess

    // methods to acess c
    void setc(int i) {
        this.c = i;
    }

    int getc() {
        return this.c;
    }
}

class AcessTest {
    public static void main(String[] args) {
        Test obj = new Test();

        // These are OK, a and b may be accessed directly
        obj.a = 10;
        obj.b = 20;

        // This is not OK and will cause an error
        //       obj.c = 100;
        // You must acees c through its methods
        obj.setc(100); // OK
        System.out.println("a, b, and c: " + obj.a +
                           " " + obj.b + " " + obj.getc());
    }
}
