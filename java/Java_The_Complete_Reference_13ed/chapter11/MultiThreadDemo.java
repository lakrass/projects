// Create multiple threads.
class NewThread implements Runnable {
    String name; // name of the thread
    Thread t;

    NewThread(String name) {
        this.name = name;
        t = new Thread(this, this.name);
        System.out.println("New thread: " + t);
    }

    // This is entry point for thread.
    public void run() {
        try {
            for (int i = 5; i > 0; i--) {
                System.out.println(this.name + ": " + i);
                Thread.sleep(1000);
            }
        } catch (InterruptedException e) {
            System.out.println(this.name + " interrupted.");
        }
        System.out.println(this.name + " exiting.");
    }
}

class MultiThreadDemo {
    public static void main(String[] args) {
        NewThread nt1 = new NewThread("One");
        NewThread nt2 = new NewThread("Two");
        NewThread nt3 = new NewThread("Three");
        NewThread nt4 = new NewThread("Four");

        nt1.t.start();
        nt2.t.start();
        nt3.t.start();
        nt4.t.start();

        try {
            Thread.sleep(10000);
        } catch (InterruptedException e) {
            System.out.println("Main thread interrupted.");
        }
        System.out.println("Exiting main thread.");
    }
}
