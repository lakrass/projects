// Suspending and resuming a thread the modern way.
class NewThread implements Runnable {
    String name; // name of the thread
    Thread t;
    boolean suspendFlag;

    NewThread(String name) {
        this.name = name;
        t = new Thread(this, this.name);
        System.out.println("New thread: " + t);
        suspendFlag = false;
    }

    // This is entry point for thread.
    public void run() {
        try {
            for(int i = 15; i > 0 ; i--) {
                System.out.println(name + ": " + i);
                Thread.sleep(200);
                synchronized(this) {
                    while(suspendFlag) {
                        wait();
                    }
                }
            }
        } catch (InterruptedException e) {
            System.out.println(name + " interrupted.");
        }
        System.out.println(name + " exiting.");
    }

    synchronized void mysuspend() {
        suspendFlag = true;
    }
    synchronized void myresume() {
        suspendFlag = false;
        notify();
    }
}
    
class SuspendResume {
    public static void main(String[] args) {
        NewThread obj1 = new NewThread("one");
        NewThread obj2 = new NewThread("two");

        obj1.t.start();
        obj2.t.start();

        try {
            Thread.sleep(1000);
            obj1.mysuspend();
            System.out.println("Suspending thread One");
            Thread.sleep(1000);
            obj1.myresume();
            System.out.println("Resuming thread One");
            obj2.mysuspend();
            System.out.println("Suspending thread Two");
            Thread.sleep(1000);
            obj2.myresume();
            System.out.println("Resuming thread Two");
        } catch (InterruptedException e) {
            System.out.println("Main thread interrupted");
        }

        // wait for threads to finish
        try {
            System.out.println("Waiting for threads to finish.");
            obj1.t.join();
            obj2.t.join();
        } catch (InterruptedException e) {
            System.out.println("Main thread interrupted.");
        }

        System.out.println("Exiting main thread.");
    }
}

