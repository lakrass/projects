class Box {
    double width, height, depth;

    // construct clone of an object
    Box(Box ob) { // pass object to constructor
        this.width = ob.width;
        this.height = ob.height;
        this.depth = ob.depth;
    }

    // constructor used when all dimensions specified
    Box(double w, double h, double d) {
        this.width = w;
        this.height = h;
        this.depth = d;
    }

    Box() {
        this.width = -1;
        this.height = -1;
        this.depth = -1;
    }

    Box(double len) {
        this.width = this.height = this.depth = len;
    }

    double volume() {
        return width * height * depth;
    }
}

// Here, Box is extended to include weight.
class BoxWeight extends Box {
    double weight;

    BoxWeight(double w, double h, double d, double m) {
        this.width = w;
        this.height = h;
        this.depth = d;
        this.weight = m;
    }
}

class DemoBoxWeight {
    public static void main(String[] args) {
        BoxWeight mybox1 = new BoxWeight(10, 20, 15, 34.3);
        var mybox2 = new BoxWeight(2, 3, 4, 0.076);

        System.out.println("Volume of mybox1 is: " + mybox1.volume());
        System.out.println("Weight of mybox1 is: " + mybox1.weight);

        System.out.println("Volume of mybox2 is: " + mybox2.volume());
        System.out.println("Weight of mybox2 is: " + mybox2.weight);
    }
}
