class Box {
    private double width, height, depth;

    Box(Box obj) {
        this.width = obj.width;
        this.height = obj.height;
        this.depth = obj.depth;
    }

    Box(double w, double h, double d) {
        this.width = w;
        this.height = h;
        this.depth = d;
    }

    Box() {
        this.width = -1;
        this.height = -1;
        this.depth = 1;
    }

    Box(double len) {
        this.width = this.height = this.depth = len;
    }

    double volume() {
        return width * height * depth;
    }
}

// BoxWeight now fully implements all constructors.
class BoxWeight extends Box {
    double weight;

    // Consturct clone of an object
    BoxWeight(BoxWeight obj) {
        super(obj);
        this.weight = obj.weight;
    }

    BoxWeight(double w, double h, double d, double m) {
        super(w, h, d);
        this.weight = m;
    }

    BoxWeight() {
        super();
        this.weight = -1;
    }

    BoxWeight(double len, double m) {
        super(len);
        this.weight = m;
    }
}

class DemoSuper {
        public static void main(String[] args) {
            BoxWeight mybox1 = new BoxWeight(10, 20, 15, 34.3);
            var mybox2 = new BoxWeight(2, 3, 4, 0.076);
            var mybox3 = new BoxWeight();
            var mycube = new BoxWeight(3, 2);
            var myclone = new BoxWeight(mybox1);

            System.out.println("Volume of mybox1 is " + mybox1.volume());
            System.out.println("Weight of mybox1 is " + mybox1.weight);
            System.out.println();

            System.out.println("Volume of mybox2 is " + mybox2.volume());
            System.out.println("Weight of mybox2 is " + mybox2.weight);
            System.out.println();

            System.out.println("Volume of mybox3 is " + mybox3.volume());
            System.out.println("Weight of mybox3 is " + mybox3.weight);
            System.out.println();

            System.out.println("Volume of mycube is " + mycube.volume());
            System.out.println("Weight of mycube is " + mycube.weight);
            System.out.println();

            System.out.println("Volume of myclone is " + myclone.volume());
            System.out.println("Weight of myclone is " + myclone.weight);
            System.out.println();
        }
}
