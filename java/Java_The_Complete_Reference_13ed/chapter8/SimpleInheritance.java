class A {
    // SUPER CLASS
    int i, j;

    void showij() {
        System.out.println("Value of i and j is: " + i + " " + j);
    }
}

class B extends A {
    // SUB CLASS
    int k;

    void showk() {
        System.out.println("Value of k is: " + k);
    }

    void sum() {
        System.out.println("Sum of i, j, k = " + (i + j + k));
    }
}

class SimpleInheritance {
    public static void main(String[] args) {
        A superObj = new A();
        B subObj = new B();

        // The superclass may be used by itself.
        superObj.i = 10;
        superObj.j = 20;
        System.out.println("Contents of superObj: ");
        superObj.showij();
        System.out.println();

        // The subclass has acess to all public members of its superclass
        subObj.i = 7;
        subObj.j = 8;
        subObj.k = 9;
        System.out.println("Contents of subObj: ");
        subObj.showij();
        subObj.showk();
        System.out.println();

        System.out.println("Sum of i, j, k in subObj: ");
        subObj.sum();
    }
}
    


