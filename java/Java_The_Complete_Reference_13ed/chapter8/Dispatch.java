class A {
    void callme() {
        System.out.println("Inside A");
    }
}

class B extends A {
    void callme() {
        System.out.println("Inside B");
    }
}

class C extends B {
    void callme() {
        System.out.println("Inside C");
    }
}

class Dispatch {
    public static void main(String[] args) {
        A a = new A();
        B b = new B();
        C c = new C();

        A r; // OBTAIN a reference of type A

        r = a;     // r refers to an A obj
        r.callme();// calls A ver of callme()
                   //
        r = b;     // r refers to an B obj
        r.callme();// B ver of callme()
                   //
        r = c;     // r refers to an C obj
        r.callme();// C ver of callme()
    }
}
