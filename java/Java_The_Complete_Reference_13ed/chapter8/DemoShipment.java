class Box {
    private double width, height, depth;

    Box(Box obj) {
        this.width = obj.width;
        this.height = obj.height;
        this.depth = obj.depth;
    }

    Box(double w, double h, double d) {
        this.width = w;
        this.height = h;
        this.depth = d;
    }

    Box() {
        this.width = -1;
        this.height = -1;
        this.depth = 1;
    }

    Box(double len) {
        this.width = this.height = this.depth = len;
    }

    double volume() {
        return width * height * depth;
    }
}

// BoxWeight now fully implements all constructors.
class BoxWeight extends Box {
    double weight;

    // Consturct clone of an object
    BoxWeight(BoxWeight obj) {
        super(obj);
        this.weight = obj.weight;
    }

    BoxWeight(double w, double h, double d, double m) {
        super(w, h, d);
        this.weight = m;
    }

    BoxWeight() {
        super();
        this.weight = -1;
    }

    BoxWeight(double len, double m) {
        super(len);
        this.weight = m;
    }
}

class Shipment extends BoxWeight {
    double cost;

    Shipment(Shipment obj) {
        super(obj);
        this.cost = obj.cost;
    }

    Shipment(double w, double h, double d, double m, double c) {
        super(w, h, d, m);
        this.cost = c;
    }

    Shipment() {
        super();
        this.cost = -1;
    }

    Shipment(double len, double m, double c) {
        super(len, m);
        this.cost = c;
    }
}

class DemoShipment {
    public static void main(String[] args) {
        Shipment ship1 = new Shipment(10, 20, 15, 10, 3.41);
        var ship2 = new Shipment(2, 3, 4, 0.76, 1.28);

        System.out.println("Volume of ship1: " + ship1.volume());
        System.out.println("Weigh of ship1: " + ship1.weight);
        System.out.println("Shipping cost of ship1: $" + ship1.cost);
        System.out.println();

        System.out.println("Volume of ship2: " + ship2.volume());
        System.out.println("Weigh of ship2: " + ship2.weight);
        System.out.println("Shipping cost of ship2: $" + ship2.cost);
        System.out.println();
    }
}
