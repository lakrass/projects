class A {
    int i, j;

    A(int a, int b) {
        this.i = a;
        this.j = b;
    }

    void show() {
        System.out.println("i and j is: " + i + " " + j);
    }
}

class B extends A {
    int k;

    B(int a, int b, int c) {
        super(a, b);
        this.k = c;
    }

//     //this override show() in A; display k
//     void show() {
//         System.out.println("k: " + k);
//     }
    void show() {
        super.show(); //this call  A's show()
        System.out.println("k: " + k);
    }
        
        
}

class Override {
    public static void main(String[] args) {
        B subObj = new B(1, 2, 3);

        subObj.show(); // this call show()in B
    }
}
