class ColorBox extends Box {
    int color; // color of box

    ColorBox(double w, double h, double d, int c) {
        this.width = w;
        this.height = h;
        this.depth = d;
        this.color = c;
    }
}
