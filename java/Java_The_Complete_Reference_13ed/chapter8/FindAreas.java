class Figure {
    double a, b;

    Figure(double a, double b) {
        this.a = a;
        this.b = b;
    }

    double area() {
        System.out.println("Area for figure is undefined.");
        return 0;
    }
}

class Triangle extends Figure {
    double c;

    Triangle(double a, double b, double c) {
        super(a, b);
        this.c = c;
    }

    double area() {
        System.out.println("Inside area for Triangle.");
        return a * b * c;
    }
}

class Rectangle extends Figure {
    double c, d;

    Rectangle(double a, double b, double c, double d) {
        super(a, b);
        this.c = c;
        this.d = d;
    }

    double area() {
        System.out.println("Inside area for Rectangle.");
        return a * b * c * d;
    }
}

class FindAreas {
    public static void main(String[] args) {
        Figure f = new Figure(10, 10);
        Rectangle r = new Rectangle(9, 5, 1, 1);
        Triangle t = new Triangle(10, 8, 1);

        Figure figref;

        figref = r;
        System.out.println("Area is " + figref.area());
        figref = t;
        System.out.println("Area is " + figref.area());
        figref = f;
        System.out.println("Area is " + figref.area());
    }
}
