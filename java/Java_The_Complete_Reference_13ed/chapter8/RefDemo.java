class RefDemo {
    public static void main(String[] args) {
        BoxWeight weighbox = new BoxWeight(3, 5, 7, 8.38);
        Box plainbox = new Box();
        double vol;

        vol = weighbox.volume();
        System.out.println("Volume of weightbox is " + vol);
        System.out.println("Weight of weightbox is " + weighbox.weight);
        System.out.println();

        // assign BoxWeigh reference to Box reference
        plainbox = weighbox;

        vol = plainbox.volume(); // OK, volume() defined in Box
        System.out.println("Volume of plainbox is " + vol);

        // The following statement is invalid because plainbox
        // does not define a weight member.
        //        System.out.println("Weight of plainbox is " + plainbox.weight);
    }
}
