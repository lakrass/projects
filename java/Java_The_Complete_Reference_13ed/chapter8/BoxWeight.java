class BoxWeight extends Box {
    // BoxWeight now uses super to initialize its Box attributes.
    double weight;

    //initialize width, height, and depth using super()
    BoxWeight(double w, double h, double d, double m) {
        super(w, h, d); // call superclass constructor
        this.weight = m;
    }
}
