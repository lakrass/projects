abstract class Figure {
    double a;
    double b;

    Figure(double a, double b) {
        this.a = a;
        this.b = b;
    }

    // area is now an abstract method
    abstract double area();
}


class Rectangle extends Figure {
    double c, d;

    Rectangle(double a, double b, double c, double d) {
        super(a, b);
        this.c = c;
        this.d = d;
    }

    double area() {
        return a * b * c * d;
    }
}

class Triangle extends Figure {
    double c;

    Triangle(double a, double b, double c) {
        super(a, b);
        this.c = c;
    }

    double area() {
        return a * b * c;
    }
}

    class AbstractAreas {
        public static void main(String[] args) {
            // Figure f = new Figure(10, 10);  ILLEGAL NOW
            Rectangle r = new Rectangle(9, 5, 1, 1);
            Triangle t = new Triangle(10, 8, 1);
            

            Figure figref; // this is OK, no object is created

            figref = r;
            System.out.println("area for rect: " + figref.area());

            figref = t;
            System.out.println("area for tri: " + figref.area());
        }
    }
