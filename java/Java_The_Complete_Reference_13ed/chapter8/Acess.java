/* In a class hierarchy, private members remain
   private to their class.

   This program contains an error and will not complie.
*/

// Create a super class
class A {
    int i; // def acess
    private int j; // private to A

    void setij(int i, int j) {
        this.i = i;
        this.j = j;
    }
}

// A's j is not accessible here.
class B extends A {
    int total;

    void sum() {
        total = i + j; // ERROR, j is NOT accessible here
    }
}

class Acess {
    public static void main(String[] args) {
        B subObj = new B();

        subObj.setij(10, 12);

        subObj.sum();
        System.out.println("Total is : " + subObj.total);
    }
}
