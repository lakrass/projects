class Exc2 {
    public static void main(String[] args) {
        int d, a;

        try { // monitor a block of code
            d = 0;
            a = 40 / d;
            System.out.println("never reach this.");
        }
        
        catch (ArithmeticException e) { // catch divide-by-zero
            System.out.println("Division by zero.");
        }

        System.out.println("After catch statement.");
    }
}
