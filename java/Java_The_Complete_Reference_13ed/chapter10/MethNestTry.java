class MethNestTry {
    static void nesttry(int a) {
        try {
            if (a == 1)
                a = a / (a - a); // div by zero

            if (a == 2) {
                int[] c = { 1 };
                c[42] = 99; // array out of bound
            }
        } catch (ArrayIndexOutOfBoundsException e) {
            System.out.println("Array oob: " + e);
        }
    }

    public static void main(String[] args) {
        try {
            int a = args.length;

            int b = 42 / a;
            System.out.println(" a = " + a);

            nesttry(a);
        } catch (ArithmeticException e) {
            System.out.println("div by 0: " + e);
        }
    }
}
