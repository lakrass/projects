class Multicatch {
    public static void main(String[] args) {
        int a = 10, b = 0;
        int[] value = { 1, 2, 3 };

        try {
            int result = a / b; // ArithmeticException

            //value[10] = 19; // ArrayIndexOutOfBoundException

            // This catch catches both exceptions
        } catch (ArithmeticException | ArrayIndexOutOfBoundsException e) {
            System.out.println("Exception caught: " + e);
        }

        System.out.println("After multi-catch");
    }
}
