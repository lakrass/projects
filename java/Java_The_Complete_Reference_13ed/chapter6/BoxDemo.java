class Box {
    double width, height, depth;
}

class BoxDemo {
    public static void main(String[] args) {
        Box mybox = new Box();
        double vol;

        // assign values to mybox's instance variables
        mybox.depth = 15;
        mybox.width = 10;
        mybox.height = 20;

        // compute volume of box
        vol = mybox.depth * mybox.height * mybox.width;

        System.out.println("Volume is " + vol);
    }
}
