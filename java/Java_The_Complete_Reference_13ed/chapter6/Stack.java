class Stack {
    // This class defines an integer stack that can hold 10 values
    int[] stack = new int[10];
    int tos;

    // Initialize top-of-stack
    Stack() {
        tos = -1;
    }

    void push(int item) {
        if (tos == 9)
            System.out.println("Stack is full.");
        else
            stack[++tos] = item;
    }

    // Pop an item from the stack
    int pop() {
        if (tos < 0) {
            System.out.println("Stack is empty!");

            return 0;
        } else
            return stack[tos--];
    }
}


/*

   8 2 0 0 0 0 0 0 0 0

   tos = 1

 */
