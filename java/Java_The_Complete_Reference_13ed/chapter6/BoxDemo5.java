class Box {
    double width, height, depth;

    // compute volume
    double volume() {
        return width * height * depth;
    }

    //void setDim(double width, double height, double depth) {
    //    this.width = width;
    //    this.height = height;
    //    this.depth = depth;
    void setDim( double w, double h, double d) {
        width = w;
        height = h;
        depth = d;
    }
}

class BoxDemo5 {
    public static void main(String[] args) {
        Box mybox1 = new Box();
        Box mybox2 = new Box();

        mybox1.setDim(10, 20, 15);
        mybox2.setDim(3, 6, 9);

        System.out.println("Volume of n1: " + mybox1.volume());
        System.out.println("Volume of n2: " + mybox2.volume());
    }
}
