class Box {
    double width, height, depth;
}

class BoxDemo2 {
    public static void main(String[] args) {
        Box mybox1 = new Box();
        Box mybox2 = new Box();
        double vol;

        //asign values to mybox1's instance variables
        mybox1.width = 10;
        mybox1.depth = 15;
        mybox1.height = 20;

        //asign values to mybox2
        mybox2.width = 3;
        mybox2.depth = 9;
        mybox2.height = 6;

        //compute volume of first box
        vol = mybox1.width * mybox1.height * mybox1.depth;
        System.out.println("Volume of first box: " + vol);
        System.out.println(mybox1.depth);

        //compute volume of second box
        vol = mybox2.width * mybox2.height * mybox2.depth;
        System.out.println("Volume of second box: " + vol);
    }
}
