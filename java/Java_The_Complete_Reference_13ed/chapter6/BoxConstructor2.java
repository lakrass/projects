class Box {
    double width;
    double height;
    double depth;

    Box(double width, double height, double depth) {
        // Use this to resolve name-space collisions
        this.width = width;
        this.height = height;
        this.depth = depth;
    }

    double volume() {
        return width * height * depth;
    }
}

class BoxConstructor2 {
    public static void main(String[] args) {
        Box mybox1 = new Box(10, 20, 15);
        Box mybox2 = new Box(3, 6, 9);

        System.out.println("1 : " + mybox1.volume());
        System.out.println("2 : " + mybox2.volume());
    }
}
